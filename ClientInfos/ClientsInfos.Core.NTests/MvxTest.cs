using ClientInfos.Core.Providers;
using Moq;
using MvvmCross.Base;
using MvvmCross.Tests;
using MvvmCross.Views;

namespace ClientInfos.Core.Tests
{
    public class MvxTest : MvxIoCSupportingTest
    {
        protected MockDispatcher MockDispatcher
        {
            get;
            private set;
        }

        public Mock<IDialogProvider> DialogMock
        {
            get;
            private set;
        }

        public MvxTest()
        {
            Setup();
            MockDispatcher = CreateMockNavigation();
        }

        ~MvxTest()
        {
            ClearAll();
        }

        protected MockDispatcher CreateMockNavigation()
        {
            var dispatcher = new MockDispatcher();
            Ioc.RegisterSingleton<IMvxMainThreadDispatcher>(dispatcher);
            Ioc.RegisterSingleton<IMvxViewDispatcher>(dispatcher);
            DialogMock = new Mock<IDialogProvider>();
            Ioc.RegisterSingleton(DialogMock.Object);
            return dispatcher;
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using ClientInfos.Core.Models.Entities;
using ClientInfos.Core.Models.Services;
using ClientInfos.Core.ViewModels.Edit;
using Moq;
using NUnit.Framework;

namespace ClientInfos.Core.Tests
{
    [TestFixture]
    public class EditViewModelTests : MvxTest
    {
        [Test]
        public void Constructor_Applies_Parameter()
        {
            ClearAll();
            var mockNavigation = CreateMockNavigation();

            var dataSourceMock = new Mock<IDataSourceService>();

            var target = new EditViewModel(dataSourceMock.Object);

            Assert.AreSame(target.DataSource, dataSourceMock.Object);
            Assert.NotNull(target.Car);
        }

        [Test]
        public void Constructor_Applies_Parameters()
        {
            ClearAll();
            var mockNavigation = CreateMockNavigation();

            var dataSourceMock = new Mock<IDataSourceService>();

            var carObject = new Car
            {
                Id = Guid.NewGuid().ToString(),
                CreationDate = DateTime.Now,
                Name = "TestName",
                Description = "TestDescription"
            };

            var target = new EditViewModel(dataSourceMock.Object, carObject);

            Assert.AreSame(target.DataSource, dataSourceMock.Object);
            Assert.NotNull(target.Car);
            Assert.True(string.Equals(target.Name, carObject.Name));
            Assert.True(string.Equals(target.Description, carObject.Description));
        }
    }
}

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ClientInfos.Core.Models.Entities;
using ClientInfos.Core.Models.Services;
using ClientInfos.Core.ViewModels.Main;
using Moq;
using MvvmCross.Navigation;
using NUnit.Framework;

namespace ClientInfos.Core.Tests
{
    [TestFixture]
    public class MainViewModelTests : MvxTest
    {
        [Test]
        public void Constructor_Applies_Parameters_To_Class_Properties()
        {
            ClearAll();
            var mockNavigation = CreateMockNavigation();

            var navigationMock = new Mock<IMvxNavigationService>();
            var dataSourceMock = new Mock<IDataSourceService>();

            var target = new MainViewModel(navigationMock.Object, dataSourceMock.Object);

            Assert.AreSame(dataSourceMock.Object, target.DataSource);
        }

        [Test]
        public async Task LoadData_Supplies_Cars_Propertie()
        {
            ClearAll();
            var mockNavigation = CreateMockNavigation();

            int numberOfItems = 10;

            var navigationMock = new Mock<IMvxNavigationService>();
            var dataSourceMock = new Mock<IDataSourceService>();

            var listOfCars = new List<Car>();

            for(int i = 0; i < numberOfItems; i++)
            {
                listOfCars.Add(new Car
                {
                    CreationDate = DateTime.Now,
                    Id = Guid.NewGuid().ToString(),
                    Description = $"{i} Test description",
                    Name = $"{i} Test name"
                });
            }

            dataSourceMock.Setup(s => s.GetCars())
                .Returns(listOfCars);

            var target = new MainViewModel(navigationMock.Object, dataSourceMock.Object);
            await target.LoadCarsAsync();

            Assert.True(((List<Car>)target.Cars).Count == numberOfItems);
        }

        [Test]
        public void CheckIfOnItemClickedUseService()
        {
            ClearAll();
            var mockNavigation = CreateMockNavigation();

            var navigationMock = new Mock<IMvxNavigationService>();
            var dataSourceMock = new Mock<IDataSourceService>();

            var target = new MainViewModel(navigationMock.Object, dataSourceMock.Object);

            target.OnItemClicked(new Car());

            DialogMock.Verify(s => s.ShowMessage(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Action>()), Times.Once);
        }
    }
}

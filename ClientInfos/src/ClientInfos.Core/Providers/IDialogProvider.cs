using System;

namespace ClientInfos.Core.Providers
{
    public interface IDialogProvider
    {
        void ShowMessage(string title, string message, string dismissButtonTitle, Action dismissed);
        void Confim(string title, string message, string okButtonTitle, string dismissButtonTitle, Action confimed, Action dismissed);
    }
}

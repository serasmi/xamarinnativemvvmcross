using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using ClientInfos.Core.Models.Entities;
using ClientInfos.Core.Models.Services;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace ClientInfos.Core.ViewModels.Main
{
    public class MainViewModel : BaseViewModel
    {
        public readonly IDataSourceService DataSource;
        public readonly IMvxNavigationService NavigationService;

        private IEnumerable<Car> _car;
        public IEnumerable<Car> Cars
        {
            get => _car;
            set
            {
                _car = value;
                RaisePropertyChanged("Cars");
            }
        }

        public ICommand ItemClicked { get; set; }

        public MainViewModel(IMvxNavigationService navigationService, IDataSourceService _dataSource)
        {
            NavigationService = navigationService;
            DataSource = _dataSource;
            Cars = new List<Car>();

            ItemClicked = new MvxCommand<Car>(OnItemClicked);
        }

        public async Task LoadCarsAsync()
        {
            Cars = DataSource.GetCars();
        }

        public void OnItemClicked(Car car)
        {
            dialogProvider.ShowMessage(car.Name, car.Description, "Dismiss", null);
        }

        public override Task Initialize()
        {
            LoadCarsAsync().ConfigureAwait(true);
            return base.Initialize();
        }
    }
}

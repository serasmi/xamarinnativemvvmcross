using System;
using ClientInfos.Core.Models.Entities;
using ClientInfos.Core.Models.Services;

namespace ClientInfos.Core.ViewModels.Edit
{
    public class EditViewModel : BaseViewModel
    {
        public IDataSourceService DataSource; 

        public Car Car;

        public string Name
        {
            get => Car.Name;
            set
            {
                Car.Name = value;
                RaisePropertyChanged("Name");
            }
        }

        public string Description
        {
            get => Car.Description;
            set
            {
                Car.Description = value;
                RaisePropertyChanged("Description");
            }
        }

        public EditViewModel(IDataSourceService dataSource)
        {
            DataSource = dataSource;

            Car = new Car
            {
                CreationDate = DateTime.Now,
                Id = Guid.NewGuid().ToString()
            };
        }

        public EditViewModel(IDataSourceService dataSource, Car car)
        {
            DataSource = dataSource;
            Car = car;
        }
    }
}

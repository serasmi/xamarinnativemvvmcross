using System.Threading.Tasks;
using ClientInfos.Core.Models.Entities;
using ClientInfos.Core.Models.Services;
using ClientInfos.Core.Models.Services.Wrappers.SecureStorage;
using ClientInfos.Core.ViewModels.Main;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace ClientInfos.Core.ViewModels.Login
{
    public class LoginViewModel : BaseViewModel
    {
        public readonly IMvxNavigationService NavigationService;
        public readonly IAuthService AuthService;
        public readonly ISecureStorageService SecureStorage;

        private string _username;
        public string Username
        {
            get => _username;
            set
            {
                _username = value;
                RaisePropertyChanged(() => Username);
            }
        }

        private string _password;
        public string Password
        {
            get => _password;
            set
            {
                _password = value;
                RaisePropertyChanged(() => Password);
            }
        }

        public IMvxAsyncCommand LogIn { get; set; }

        private bool _isEnabled = true;
        public bool IsEnabled
        {
            get => _isEnabled;
            set
            {
                _isEnabled = value;
                RaisePropertyChanged(() => IsEnabled);
            }
        }

        public LoginViewModel(IMvxNavigationService navigationService, IAuthService authService, ISecureStorageService secureStorageService)
        {
            NavigationService = navigationService;
            AuthService = authService;
            SecureStorage = secureStorageService;
        }

        public async Task<bool> AuthenticateAsync()
        {
            if (await AuthService.Authenticate(Username, Password) is User _user && _user != null && !string.IsNullOrWhiteSpace(_user.Token))
            {
                await SecureStorage.Set(UserFieldsEnum.Username, _user.Username);
                await SecureStorage.Set(UserFieldsEnum.Token, _user.Token);
                await SecureStorage.Set(UserFieldsEnum.Code, _user.Code);
                return true;
            }
            else
            {
                return false;
            }
        }

        private Task<bool> ChangePage()
        {
            if(AuthenticateAsync().ConfigureAwait(true).GetAwaiter().GetResult())
            {
                return NavigationService.Navigate<MainViewModel>();
            }

            return Task.FromResult(false);
        }

        public override void Prepare()
        {
            LogIn = new MvxAsyncCommand(ChangePage);
        }
    }
}

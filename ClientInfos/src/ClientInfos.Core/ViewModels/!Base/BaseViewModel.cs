using System;
using System.Collections.Generic;
using System.Text;
using ClientInfos.Core.Providers;
using MvvmCross;
using MvvmCross.ViewModels;

namespace ClientInfos.Core.ViewModels
{
    public abstract class BaseViewModel : MvxViewModel
    {
        protected IDialogProvider dialogProvider;

        public BaseViewModel()
        {
            dialogProvider = Mvx.IoCProvider.Resolve<IDialogProvider>();
        }
    }
}

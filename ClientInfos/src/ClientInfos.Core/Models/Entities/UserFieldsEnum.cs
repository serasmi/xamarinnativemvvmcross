using System.ComponentModel;

namespace ClientInfos.Core.Models.Entities
{
    public enum UserFieldsEnum
    {
        [Description("Code")]
        Code,
        [Description("Username")]
        Username,
        [Description("Token")]
        Token,
        [Description("Password")]
        Password,
        [Description("Token_Expire_Date")]
        TokenExpireDate
    }
}

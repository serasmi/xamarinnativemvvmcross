using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace ClientInfos.Core.Models.Helpers
{
    public static class EnumDescriptionHelper
    {
        public static string GetDescription(object enumObj)
        {
            var fi = enumObj.GetType().GetField(enumObj.ToString());
            var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return (attributes.Length > 0) ? attributes[0].Description : enumObj.ToString();
        }

        public static string Description(this Enum value)
        {
            return GetDescription(value);
        }
    }
}

using System.Threading.Tasks;
using ClientInfos.Core.Models.Entities;

namespace ClientInfos.Core.Models.Services
{
    public interface IAuthService
    {
        Task<User> Authenticate(string username, string password);
    }
}

using System.Threading.Tasks;
using ClientInfos.Core.Models.Entities;

namespace ClientInfos.Core.Models.Services.Wrappers.SecureStorage
{
    public class SecureStorageService : ISecureStorageService
    {
        public Task<string> Get(string key)
        {
            throw new System.NotImplementedException();
        }

        public Task<bool> LoggedIn()
        {
            return Task.FromResult(false);
        }

        public async Task RemoveAll()
        {
            return;
        }

        public async Task Set(UserFieldsEnum key, string value)
        {
            return;
        }
    }
}

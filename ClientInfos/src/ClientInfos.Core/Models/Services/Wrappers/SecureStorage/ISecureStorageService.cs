using System.Threading.Tasks;
using ClientInfos.Core.Models.Entities;

namespace ClientInfos.Core.Models.Services.Wrappers.SecureStorage
{
    public interface ISecureStorageService
    {
        Task<bool> LoggedIn();
        Task<string> Get(string key);
        Task Set(UserFieldsEnum key, string value);
        Task RemoveAll();
    }
}

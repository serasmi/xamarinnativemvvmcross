using System.Collections.Generic;
using ClientInfos.Core.Models.Entities;

namespace ClientInfos.Core.Models.Services
{
    public interface IDataSourceService
    {
        List<Car> GetCars();
    }
}

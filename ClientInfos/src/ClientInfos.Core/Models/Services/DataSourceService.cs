using System;
using System.Collections.Generic;
using System.Text;
using ClientInfos.Core.Models.Entities;

namespace ClientInfos.Core.Models.Services
{
    public class DataSourceService : IDataSourceService
    {
        private readonly List<Car> _cars = new List<Car>();

        public DataSourceService()
        {
            _cars.Add(new Car
            {
                Id = Guid.NewGuid().ToString(),
                CreationDate = DateTime.Now,
                Description = "This is first car added",
                Name = "First car"
            });

            _cars.Add(new Car
            {
                Id = Guid.NewGuid().ToString(),
                CreationDate = DateTime.Now,
                Description = "This is second car added",
                Name = "Second car"
            });

            _cars.Add(new Car
            {
                Id = Guid.NewGuid().ToString(),
                CreationDate = DateTime.Now,
                Description = "This is third car added",
                Name = "Third car"
            });
        }

        public List<Car> GetCars()
        {
            return _cars;
        }
    }
}

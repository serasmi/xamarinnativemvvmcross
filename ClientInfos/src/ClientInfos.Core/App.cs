using MvvmCross.IoC;
using MvvmCross.ViewModels;
using ClientInfos.Core.ViewModels.Main;
using ClientInfos.Core.ViewModels.Login;

namespace ClientInfos.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();
                       
            RegisterAppStart<LoginViewModel>();
        }
    }
}

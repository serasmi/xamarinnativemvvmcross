using System;
using System.Globalization;
using MvvmCross.Converters;

namespace ClientInfos.Core.Converters
{
    public class DateTimeToStringConverter : MvxValueConverter<DateTime, string>
    {
        protected override string Convert(DateTime value, Type targetType, object parameter, CultureInfo culture)
        {
            return value.ToShortDateString();
        }
    }
}

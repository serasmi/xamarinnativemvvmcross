using System;
using Android.App;
using ClientInfos.Core.Providers;
using MvvmCross;
using MvvmCross.Platforms.Android;

namespace ClientInfos.Droid.Providers
{
    public class DroidDialogProvider : IDialogProvider
    {
        public void Confim(string title, string message, string okButtonTitle, string dismissButtonTitle, Action confimed, Action dismissed)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(Mvx.IoCProvider.Resolve<IMvxAndroidCurrentTopActivity>().Activity);
            AlertDialog alertDialog = builder.Create();
            builder.SetTitle(title);
            builder.SetMessage(message);
            builder.SetNegativeButton(dismissButtonTitle, (sender, args) =>
            {
                if (dismissed != null)
                    dismissed.Invoke();
            });
            builder.SetPositiveButton(okButtonTitle, (sender, args) =>
            {
                if (confimed != null)
                    confimed.Invoke();
            });
            builder.Show();
        }

        public void ShowMessage(string title, string message, string dismissButtonTitle, Action dismissed)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(Mvx.IoCProvider.Resolve<IMvxAndroidCurrentTopActivity>().Activity);
            AlertDialog alertDialog = builder.Create();
            builder.SetTitle(title);
            builder.SetMessage(message);
            builder.SetNegativeButton(dismissButtonTitle, (sender, args) =>
            {
                if (dismissed != null)
                    dismissed.Invoke();
            });
            builder.Show();
        }
    }
}

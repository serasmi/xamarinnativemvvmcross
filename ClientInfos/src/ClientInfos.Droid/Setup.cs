using MvvmCross.Droid.Support.V7.AppCompat;
using ClientInfos.Core;
using MvvmCross;
using MvvmCross.IoC;
using ClientInfos.Core.Providers;
using ClientInfos.Droid.Providers;

namespace ClientInfos.Droid
{
    public class Setup : MvxAppCompatSetup<App>
    {
        protected override IMvxIoCProvider InitializeIoC()
        {
            base.InitializeIoC();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IDialogProvider, DroidDialogProvider>();
            return Mvx.IoCProvider;
        }
    }
}

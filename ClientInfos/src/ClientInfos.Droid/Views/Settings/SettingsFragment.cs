using MvvmCross.Platforms.Android.Presenters.Attributes;
using ClientInfos.Core.ViewModels.Main;
using ClientInfos.Core.ViewModels.Settings;

namespace ClientInfos.Droid.Views.Settings
{
    [MvxFragmentPresentation(typeof(MainContainerViewModel), Resource.Id.content_frame, true)]
    public class SettingsFragment : BaseFragment<SettingsViewModel>
    {
        protected override int FragmentLayoutId => Resource.Layout.fragment_settings;
    }
}

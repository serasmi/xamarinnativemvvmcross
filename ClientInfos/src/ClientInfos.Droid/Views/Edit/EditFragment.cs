using Android.OS;
using Android.Views;
using Android.Widget;
using ClientInfos.Core.ViewModels.Edit;
using ClientInfos.Core.ViewModels.Main;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Platforms.Android.Presenters.Attributes;

namespace ClientInfos.Droid.Views.Edit
{
    [MvxFragmentPresentation(typeof(MainContainerViewModel), Resource.Id.content_frame, true)]
    public class EditFragment : BaseFragment<EditViewModel>
    {
        protected override int FragmentLayoutId => Resource.Layout.fragment_edit;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var _ = base.OnCreateView(inflater, container, savedInstanceState);

            var view = this.BindingInflate(FragmentLayoutId, container, false);

            var nameEdit = view.FindViewById<EditText>(Resource.Id.edit_name);
            var descriptionEdit = view.FindViewById<EditText>(Resource.Id.edit_description);

            var set = this.CreateBindingSet<EditFragment, EditViewModel>();

            set.Bind(nameEdit).For(v => v.Text).To(vm => vm.Name).TwoWay();
            set.Bind(descriptionEdit).For(v => v.Text).To(vm => vm.Description).TwoWay();

            set.Apply();

            return view;
        }
    }
}

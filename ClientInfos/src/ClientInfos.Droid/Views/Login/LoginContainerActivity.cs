using MvvmCross.Droid.Support.V7.AppCompat;
using ClientInfos.Core.ViewModels.Login;
using ClientInfos.Droid.Views.Helpers;
using Android.OS;
using Android.App;
using Android.Views;

namespace ClientInfos.Droid.Views.Login
{
    [Activity(
        Theme = "@style/AppTheme",
        WindowSoftInputMode = SoftInput.AdjustResize | SoftInput.StateHidden)]
    public class LoginContainerActivity : BaseActivity<LoginContainerViewModel>
    {
        protected override int ActivityLayoutId => Resource.Layout.activity_login_container;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
        }
    }
}

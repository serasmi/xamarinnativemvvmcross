using Android.OS;
using Android.Views;
using Android.Widget;
using ClientInfos.Core.ViewModels.Login;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Platforms.Android.Presenters.Attributes;

namespace ClientInfos.Droid.Views.Login
{
    [MvxFragmentPresentation(typeof(LoginContainerViewModel), Resource.Id.content_frame, true)]
    public class LoginFragment : BaseFragment<LoginViewModel>
    {
        protected override int FragmentLayoutId => Resource.Layout.fragment_login;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var _= base.OnCreateView(inflater, container, savedInstanceState);

            var view = this.BindingInflate(FragmentLayoutId, container, false);

            var usernameEdit = view.FindViewById<EditText>(Resource.Id.usernameEditText);
            var passwordEdit = view.FindViewById<EditText>(Resource.Id.passwordEditText);

            var set = this.CreateBindingSet<LoginFragment, LoginViewModel>();

            set.Bind(usernameEdit).For(v => v.Text).To(vm => vm.Username).TwoWay();
            set.Bind(passwordEdit).For(v => v.Text).To(vm => vm.Password).TwoWay();

            set.Apply();
            return view;
        }
    }
}

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Threading.Tasks;
using ClientInfos.Core.Models.Entities;
using ClientInfos.Core.Models.Services;
using ClientInfos.Core.Providers;
using ClientInfos.Core.ViewModels.Main;
using Moq;
using MvvmCross.Base;
using MvvmCross.Tests;
using MvvmCross.Views;
using Xunit;

namespace ClientInfos.Core.Tests
{
    public class MainViewModelTests : IClassFixture<MvxTest>
    {
        private readonly MvxTest _test;

        public MainViewModelTests(MvxTest mvx)
        {
            _test = mvx;
        }

        [Fact]
        public void Constructor_Applies_Parameters_To_Class_Properties()
        {
            //ClearAll();
            //var mockNavigation = CreateMockNavigation();

            var dataSourceMock = new Mock<IDataSourceService>();

            var target = new MainViewModel(dataSourceMock.Object);

            Assert.Equal(dataSourceMock.Object, target.DataSource);
        }

        [Fact]
        public async Task LoadData_Supplies_Cars_Propertie()
        {
            //ClearAll();
            //var mockNavigation = CreateMockNavigation();

            int numberOfItems = 10;

            var dataSourceMock = new Mock<IDataSourceService>();

            var listOfCars = new List<Car>();

            for(int i = 0; i < numberOfItems; i++)
            {
                listOfCars.Add(new Car
                {
                    CreationDate = DateTime.Now,
                    Id = Guid.NewGuid().ToString(),
                    Description = $"{i} Test description",
                    Name = $"{i} Test name"
                });
            }

            dataSourceMock.Setup(s => s.GetCars())
                .Returns(listOfCars);

            var target = new MainViewModel(dataSourceMock.Object);
            await target.LoadCarsAsync();

            Assert.True(((List<Car>)target.Cars).Count == numberOfItems);
        }

        [Fact]
        public void CheckIfOnItemClickedUseService()
        {
            //ClearAll();
            //var mockNavigation = CreateMockNavigation();

            var dataSourceMock = new Mock<IDataSourceService>();

            var target = new MainViewModel(dataSourceMock.Object);

            target.OnItemClicked(new Car());

            _test.DialogMock.Verify(s => s.ShowMessage(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Action>()), Times.Once);
        }
    }
}

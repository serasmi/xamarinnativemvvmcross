using System.Threading.Tasks;
using ClientInfos.Core.Models.Entities;
using ClientInfos.Core.Models.Services;
using ClientInfos.Core.Models.Services.Wrappers.SecureStorage;
using ClientInfos.Core.ViewModels.Login;
using Moq;
using MvvmCross.Navigation;
using Xunit;

namespace ClientInfos.Core.Tests
{
    public class LoginViewModelTests : IClassFixture<MvxTest>
    {
        [Fact]
        public void Constructor_Applies_Parameters_To_Global_Variables()
        {
            //ClearAll();
            //var mockNavigation = CreateMockNavigation();

            var navigationServiceMock = new Mock<IMvxNavigationService>();
            var authServiceMock = new Mock<IAuthService>();
            var secureStorageMock = new Mock<ISecureStorageService>();

            var lvm = new LoginViewModel(navigationServiceMock.Object, authServiceMock.Object, secureStorageMock.Object);

            Assert.Equal(authServiceMock.Object, lvm.AuthService);
            Assert.Equal(secureStorageMock.Object, lvm.SecureStorage);
        }

        [Fact]
        public void Check_If_Username_Field_Applies_Value_To_User_Object()
        {
            //ClearAll();
            //var mockNavigation = CreateMockNavigation();

            var navigationServiceMock = new Mock<IMvxNavigationService>();
            var authServiceMock = new Mock<IAuthService>();
            var secureStorageMock = new Mock<ISecureStorageService>();

            var lvm = new LoginViewModel(navigationServiceMock.Object, authServiceMock.Object, secureStorageMock.Object)
            {
                Username = "test"
            };

            Assert.Equal("test", lvm.Username);
        }

        [Fact]
        public void Check_If_Password_Field_Applies_Value()
        {
            //ClearAll();
            //var mockNavigation = CreateMockNavigation();

            var navigationServiceMock = new Mock<IMvxNavigationService>();
            var authServiceMock = new Mock<IAuthService>();
            var secureStorageMock = new Mock<ISecureStorageService>();

            var lvm = new LoginViewModel(navigationServiceMock.Object, authServiceMock.Object, secureStorageMock.Object)
            {
                Password = "test"
            };

            Assert.Equal("test", lvm.Password);
        }

        [Fact]
        public async void Authenticate_Return_True()
        {
            //ClearAll();
            //var mockNavigation = CreateMockNavigation();

            var navigationServiceMock = new Mock<IMvxNavigationService>();
            var authServiceMock = new Mock<IAuthService>();
            authServiceMock.Setup(s => s.Authenticate(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new User { Token = "Test" }));
            var secureStorageMock = new Mock<ISecureStorageService>();

            var lvm = new LoginViewModel(navigationServiceMock.Object, authServiceMock.Object, secureStorageMock.Object);

            Assert.True(await lvm.AuthenticateAsync());
        }

        [Fact]
        public async void Authenticate_Return_False()
        {
            //ClearAll();
            //var mockNavigation = CreateMockNavigation();

            var navigationServiceMock = new Mock<IMvxNavigationService>();
            var authServiceMock = new Mock<IAuthService>();
            authServiceMock.Setup(s => s.Authenticate(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new User()));
            var secureStorageMock = new Mock<ISecureStorageService>();

            var lvm = new LoginViewModel(navigationServiceMock.Object, authServiceMock.Object, secureStorageMock.Object);

            Assert.False(await lvm.AuthenticateAsync());
        }

        [Fact]
        public async void Authenticate_Use_Service()
        {
            //ClearAll();
            //var mockNavigation = CreateMockNavigation();

            var navigationServiceMock = new Mock<IMvxNavigationService>();
            var authServiceMock = new Mock<IAuthService>();
            authServiceMock.Setup(s => s.Authenticate(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new User { Token = "Test" }));
            var secureStorageMock = new Mock<ISecureStorageService>();

            var lvm = new LoginViewModel(navigationServiceMock.Object, authServiceMock.Object, secureStorageMock.Object);
            await lvm.AuthenticateAsync();

            authServiceMock.Verify(s => s.Authenticate(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }
    }
}
